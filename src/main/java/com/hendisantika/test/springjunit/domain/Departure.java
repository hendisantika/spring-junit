package com.hendisantika.test.springjunit.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/20/17
 * Time: 11:12 AM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "departure")
public class Departure {
    @Id
    @Column
    @GeneratedValue
    private int id;

    @Column
    private String city;

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
