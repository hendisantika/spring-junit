package com.hendisantika.test.springjunit.repository;

import com.hendisantika.test.springjunit.domain.Flight;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/20/17
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
public interface FlightRepository extends PagingAndSortingRepository<Flight, Integer> {
    List<Flight> findAll();

    Flight findAllById(int id);
}
