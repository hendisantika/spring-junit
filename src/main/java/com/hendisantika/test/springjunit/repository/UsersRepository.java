package com.hendisantika.test.springjunit.repository;

import com.hendisantika.test.springjunit.domain.Users;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/20/17
 * Time: 11:20 AM
 * To change this template use File | Settings | File Templates.
 */
public interface UsersRepository extends PagingAndSortingRepository<Users, Integer> {
    List<Users> findAll();

    Users findAllById(int id);

    @Transactional
    void deleteById(int id);
}
