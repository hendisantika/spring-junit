package com.hendisantika.test.springjunit.repository;

import com.hendisantika.test.springjunit.domain.Arrival;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/20/17
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ArrivalRepository extends PagingAndSortingRepository<Arrival, Integer> {
    List<Arrival> findAll();

    Arrival findAllById(int id);
}
