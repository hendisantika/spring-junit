package com.hendisantika.test.springjunit.repository;

import com.hendisantika.test.springjunit.domain.Departure;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/20/17
 * Time: 11:18 AM
 * To change this template use File | Settings | File Templates.
 */
public interface DepartureRepository extends PagingAndSortingRepository<Departure, Integer> {
    List<Departure> findAll();

    Departure findAllById(int id);
}
