package com.hendisantika.test.springjunit.bdd.steps;

import com.hendisantika.test.springjunit.bdd.FeaturedTest;
import cucumber.api.java.en.When;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/21/17
 * Time: 6:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserSteps extends FeaturedTest {
    private static final Logger LOGGER = getLogger(UserSteps.class);

    @When("^User gets one user by id (\\d+)$")
    public void userGetsOneUserById(int id) throws Throwable {
        LOGGER.info("When - User gets one user by id [{}]", id);
    }
}
