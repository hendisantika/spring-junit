package com.hendisantika.test.springjunit.bdd.steps;

import com.hendisantika.test.springjunit.bdd.FeaturedTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/21/17
 * Time: 6:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommonSteps extends FeaturedTest {
    private static final Logger LOGGER = getLogger(CommonSteps.class);

    @Given("^(.+) rest endpoint is up$")
    public void arrivalRestEndpointIsUp(String endpointType) throws Throwable {
        LOGGER.info("Given - [{}] rest endpoint is up", endpointType);
    }

    @Then("^Returned JSON object is not null$")
    public void returnedJSONObjectIsNotNull() {
        LOGGER.info("Then - Returned JSON object is not null");
    }
}
