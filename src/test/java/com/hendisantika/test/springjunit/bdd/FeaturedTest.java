package com.hendisantika.test.springjunit.bdd;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/21/17
 * Time: 6:23 AM
 * To change this template use File | Settings | File Templates.
 */

import com.hendisantika.test.springjunit.SpringJunitApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
@SpringBootTest(
        classes = SpringJunitApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public abstract class FeaturedTest {
}
