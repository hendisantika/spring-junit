package com.hendisantika.test.springjunit.bdd;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/21/17
 * Time: 6:22 AM
 * To change this template use File | Settings | File Templates.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"com.demo.bdd.steps"},
        features = {"classpath:bdd/features"}
)
@ActiveProfiles(value = "test")
public class BddCoverage {

}
